/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.util.Arrays;

/**
 *
 * @author madarme
 */
public class ManejoVector_Generico<T> {
    
    private T []vector;

    public ManejoVector_Generico() {
    }

    public ManejoVector_Generico(T[] vector) {
        this.vector = vector;
    }

    public T[] getVector() {
        return vector;
    }

    public void setVector(T[] vector) {
        this.vector = vector;
    }

    @Override
    public String toString() {
       
         String msg="Vector de :"+this.getClass().getSimpleName()+": ";
        for(T datos:this.vector)
        {
            msg+=datos.toString()+"\t";
        }
        
        return msg;
        
        
        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
       
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ManejoVector_Generico<T> other = (ManejoVector_Generico<T>) obj;
        
        if(this.vector.length!=other.vector.length)
            return false;
        
        for(int i=0; i<this.vector.length;i++)
        {
            if(! this.vector[i].equals(other.vector[i]))
                return false;
        }
        
        return true;
       
        
        
        
    }
    
    
    /**
     * Ordena el vector en sí mismo usando el método de ordenamiento de inserción, puede tomar como ejemplo la siguiente url:
     * https://juncotic.com/ordenamiento-por-insercion-algoritmos-de-ordenamiento/#:~:text=El%20algoritmo%20de%20ordenamiento%20por%20inserci%C3%B3n%20es%20un%20algoritmo%20de,insert%C3%A1ndolo%20en%20el%20lugar%20correspondiente.
     * 
     * Recomendación: Implementar el método compareTo en la clase Persona 
     */
    public void ordenarInsercion(){
        
        int k, j;
        T aux;
        
        for( k = 1; k < vector.length; k++){
            aux = this.vector[k];
            Comparable compareToObj = (Comparable)aux;
            for(j = k - 1; j >= 0 && compareToObj.compareTo(vector[j])< 0; j--){
                T obj1 = vector[j];
                vector[j+1] = (obj1);
                vector[j] = (aux);
            }
        }
    }
    
    
    
    
    
}
