/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class Persona implements Comparable {

    private int cedula;
    private String nombre;
    private int d, m, a; //Fecha de nacimiento

    public Persona() {
    }

    public Persona(int cedula, String nombre, int d, int m, int a) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.d = d;
        this.m = m;
        this.a = a;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public int getM() {
        return m;
    }

    public void setM(int m) {
        this.m = m;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula + ", nombre=" + nombre + ", d=" + d + ", m=" + m + ", a=" + a + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.cedula;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (this.cedula != other.cedula) {
            return false;
        }
        return true;
    }

    /**
     * Comparar por fecha de nacimiento
     *
     * @param o objeto persona
     * @return <1 si es this < obj, 0 si son iguales o >0 si this>obj
     */
    @Override
    public int compareTo(Object o) {

        final Persona other = (Persona) o;
        int resultado = 0;

        if (other.a <= this.a) {
            if (other.a < this.a) {
                resultado = 1;
            }
            if (other.m < this.m) {
                resultado = 1;
            }
            if (other.d < this.d) {
                resultado = 1;
            }
        }

        if (other.a >= this.a) {
            if (other.a > this.a) {
                resultado = -1;
            }
            if (other.m > this.m) {
                resultado = -1;
            }
            if (other.d > this.d) {
                resultado = -1;
            }
        }

        if (other.a == this.a && other.m == this.m && other.d == this.d) {
            resultado = 0;
        }
        return resultado;
    }

}
